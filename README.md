# Sintezilo

Sintzilo is project that utilizes SonicPi and input to make different synth chords.
Currently only keyboard input is accepted, but there are plans to include DDR dance mats and rockband guitars.

#Playing
SonicPi must be running before running this program.
Basic operation is pressing a key (anything from a, b, ... f, g) and getting the corresponding major chord.
Holding the 'm' key at the same time turns the major chord into a minor.
Pressing up raises it an octave and pressing down lowers it by an octave.