# This only works when I import from pynput.keyboard and pynput, but I'm dumb.
from pynput.keyboard import Key, Listener
from pynput import keyboard
from psonic import *

# This represents the current octave
octv = 4
# This represents the type of chord to be played
chord_type = MAJOR


def on_press(key):
    global octv
    global chord_type
    if key == Key.esc:
        # Stops listener
        return False
    if key == keyboard.KeyCode(char='m'):
        # This makes it so that a minor chord is played
        chord_type = MINOR
    if key == Key.up:
        # Python-Sonic cannot handle octaves greater than 6
        if octv < 6:
            # Increases chord octave by 1
            octv += 1
            print(octv)
    if key == Key.down:
        # Python-Sonic cannot handle octaves less than 2
        if octv > 2:
            # Decreases chord octave by 1
            octv -= 1
            print(octv)
    if key == keyboard.KeyCode(char='a'):
        # This sets default chord to be played
        chrd = A4
        # This sets up the what kind of chord to play and plays it
        # It is currently a bit of a mess and I want to simplify it
        if octv != 4:
            if octv == 2:
                chrd = A2
            if octv == 3:
                chrd = A3
            if octv == 5:
                chrd = A5
            if octv == 6:
                chrd = A5
        # Plays the A chord
        play(chord(chrd, chord_type))
        # Logs the chord that is played. It is off because of octave limitations when displaying 6th octave
        print('A' + str(octv) + str(chord_type).title())
    if key == keyboard.KeyCode(char='b'):
        chrd = B4
        if octv != 4:
            if octv == 2:
                chrd = B2
            if octv == 3:
                chrd = B3
            if octv == 5:
                chrd = B5
            if octv == 6:
                chrd = B5
        # Plays the B chord
        play(chord(chrd, chord_type))
        print('B' + str(octv) + str(chord_type).title())
    if key == keyboard.KeyCode(char='c'):
        chrd = C4
        if octv != 4:
            if octv == 2:
                chrd = C2
            if octv == 3:
                chrd = C3
            if octv == 5:
                chrd = C5
            if octv == 6:
                chrd = C6
        # Plays the C chord
        play(chord(chrd, chord_type))
        print('C' + str(octv) + str(chord_type).title())
    if key == keyboard.KeyCode(char='d'):
        chrd = D4
        if octv != 4:
            if octv == 2:
                chrd = D2
            if octv == 3:
                chrd = D3
            if octv == 5:
                chrd = D5
            if octv == 6:
                chrd = D5
        # Plays the D chord
        play(chord(chrd, chord_type))
        print('D' + str(octv) + str(chord_type).title())
    if key == keyboard.KeyCode(char='e'):
        chrd = E4
        if octv != 4:
            if octv == 2:
                chrd = E2
            if octv == 3:
                chrd = E3
            if octv == 5:
                chrd = E5
            if octv == 6:
                chrd = E5
        # Plays the E chord
        play(chord(chrd, chord_type))
        print('E' + str(octv) + str(chord_type).title())
    if key == keyboard.KeyCode(char='f'):
        chrd = F4
        if octv != 4:
            if octv == 2:
                chrd = F2
            if octv == 3:
                chrd = F3
            if octv == 5:
                chrd = F5
            if octv == 6:
                chrd = F5
        # Plays the F chord
        play(chord(chrd, chord_type))
        print('F' + str(octv) + str(chord_type).title())
    if key == keyboard.KeyCode(char='g'):
        chrd = G4
        if octv != 4:
            if octv == 2:
                chrd = G2
            if octv == 3:
                chrd = G3
            if octv == 5:
                chrd = G5
            if octv == 6:
                chrd = G5
        # Plays the G chord
        play(chord(chrd, chord_type))
        print('G' + str(octv) + str(chord_type).title())


def on_release(key):
    global chord_type
    if key == keyboard.KeyCode(char='m'):
        chord_type = MAJOR


# Collect events until released
with Listener(on_press=on_press, on_release=on_release) as listener:
    listener.join()
